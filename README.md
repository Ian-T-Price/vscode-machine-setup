# ubuntu-dev-machine-setup | Ubuntu 20.04 LTS

This repo contains one Ansible playbook to configure VS Code with extentions.

It was hacked from a much larger playbook so you may find some redundant artefacts lying around

The playbooks should run in Debian based system but was only tested with:
- **Ubuntu 20.04**

## Pre-requisites

On the system which you are going to setup using Ansible, perform these steps.

You need to install `ansible` and `git` before running the playbooks. You can either install it using `pip` or `apt`.

```
sudo apt install ansible git
```

And clone this repo

```
git clone https://gitlab.com/Ian-T-Price/vscode-machine-setup
cd vscode-machine-setup
```

## Running the playbook to configure your system

**Invoke the following as yourself, the primary user of the system. Do not run as `root`.**

```
ansible-playbook main.yml -e "local_username=$(id -un)" -K
```
The local username is your login name - it can be made permanent by editing group_vars/all/all.yml

Enter the sudo password when asked for `BECOME password:`.

The `main.yml` playbook will take anything from 1-4 minutes to complete.


## What gets installed and cofigured?

I am a Linux Systems Engineer and my daily job include working with various config management using Ansible. So if you are in a similar profession the installed system will suit your needs. It is also easy to extend using Ansible roles.

Summary of packages that get installed and configured:

- Visual Studio Code and some popular extensions

## Known Issues

- If the ansible playbook halts after completing a few tasks, simply run the playbook again. Since most of the tasks are idempotent, running the playbook multiple times won't break anything.
- If your terminal shows any weird characters because of installing one of the zsh themes, simply change the font to a suitable Nerd Font from the terminal's settings.

## Pull Requests and Forks

You are more than welcome to send any pull requests. However, the intention of this repo is to suit my development needs. So it might be better if you *fork* this repo instead for your own needs and personalization.

## Useful Links

[ubuntu-dev-machine-setup | Ubuntu 20.04 LTS](https://github.com/fazlearefin/ubuntu-dev-machine-setup.git)
[Writing Ansible playbook with Visual Studio Code](https://www.golinuxcloud.com/visual-studio-code-with-ansible/)
[How to install software with Ansible](https://opensource.com/article/20/9/install-packages-ansible)

## TODOs

- Resize VS Code font
